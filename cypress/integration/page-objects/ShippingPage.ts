import PaymentPage from "./PaymentPage";

export default class ShippingPage {
  _proceedCta = () => cy.get('#form > p > button > span');
  _conditionsCheckbox = () => cy.get('#cgv');
 
  acceptTermsAndConditions() {
    this._conditionsCheckbox().trigger('mouseover').click();
    return this;
  }

  goToPaymentPage() {
      this._proceedCta().click();
      return new PaymentPage();
  }
 
}
